# aurora-focus

Expérimentation de charte graphique Scenari / Opale basée sur Aurora, qui a pour objectif de faciliter la lisibilité et l'ergonomie de consultation des contenus.

Principaux changements :

 - allégement : moins de bordures, moins d'encadrés, plus d'interlignes, de marge entre les paragraphes, plus de blanc
 - largeur de ligne maximum (85em), sans débordement lorsque la fenêtre descend sous cette taille
 - suppression de scrollbar interne au contenu et du mécanisme de scroll par boutons pour le menu de navigation
 - autres micro-ajustements (meilleur support d'un h1 sur 2 lignes, test d'autres polices)
 - le reste a pour conséquence des modifs dans la manière dont le responsive design est codé (a retester, je ne suis pas sûr de bien avoir repris toutes les fonctionnalités d'aurora...)

Développeur principal (options d'accessibilité Aspie-Friendly) : Antoine Kryus

Contact: Stéphane Poinsart &lt;stephane.poinsart@utc.fr&gt;
